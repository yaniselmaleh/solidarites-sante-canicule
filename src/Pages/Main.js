import React, { Component } from 'react';
import Home from "../Components/Front/Home";
import Informations from '../Components/Front/Informations';
import Animal from '../Components/Front/Animal';
import Numeros from '../Components/Front/Numeros';

class Main extends Component {  

  render(){
    return (
      <>
          <Home/>
          <Informations/>
          <Animal/>
          <Numeros/>
      </>
    );
  }
}

export default Main