import React, {useEffect, useState} from 'react';
import {getData} from './getData'
import Loader from "../Loader/Loader"
import {Switch} from "../Utils/Switch.js"


function ApiMeteo () {
    const [data, setData] = useState([]);
    const [load, setLoad] = useState(false);
    const [error, setError] = useState(null);
    const BASE_URL = 'http://api.openweathermap.org/data/2.5/weather?q=Paris&lang=fr&units=metric'
    const API_KEY  = '&appid=ea4087f625665cd9f2cbbda61d3a245d'
  
    useEffect(() => {
        // getData(`http://api.openweathermap.org/data/2.5/weather?q=Paris&lang=fr&units=metric&appid=ea4087f625665cd9f2cbbda61d3a245d`)
        getData(`${BASE_URL+API_KEY}`)
          .then(res => {
            setData(res);
            setLoad(true)
        }).catch(err => {
                setError(err);
                setLoad(true);
            }
        );
    }, []);


    if (load) {
        return (
                    <ul>
                        {
                            error 
                            ? <li>{error.message}</li>
                            : [data].map((GetData, index) =>
                                <li key={index}>
                                    Température : {Math.round(GetData.main.temp) + " °c "}
                                    <br/>
                                    il fait {Switch(Math.round(GetData.main.temp))}

                                    {/* {console.log(GetData.main.temp)} */}
                                    {/* {GetData.main.temp<= 18 ? <p>Inférieur</p> : <p>Supérieur</p>} */}
                                    {/* {console.log(Switch(2))} */}
                                    {console.log("Valeur du switch : " + Switch(Math.round(GetData.main.temp)))}
                                </li>)
                        }
                    </ul>
        );
    } 
    
    else {
        return (
            <>
                <Loader/>
            </>
        );
    }
};

export default ApiMeteo;
