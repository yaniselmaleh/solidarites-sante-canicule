import React, {Component, lazy, Suspense} from 'react'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

// Spinner (Chargement en Suspens)
import Spinner from "../Components/Loader/Spinner";
import "../Sass/Components/_reset.scss";
import "../Sass/Components/_font.scss";

const Main = lazy(() => import("../Pages/Main"));

class Rooter extends Component {
    render() {
        
        const Root = () => (
            <>
                <Switch>
                    <Suspense fallback={<Spinner/>}>
                        <Route exact path="/" component={Main}/>
                    </Suspense>
                </Switch>
            </>
        )

        return (
            <Router>
                <Root/>
            </Router>
        )
    }
}

export default Rooter