import React from 'react'
import { useSpring, animated } from 'react-spring'
import { Link } from 'react-scroll'
import "../../Sass/Components/_home.scss";
import "../../Sass/Components/_cta.scss";
import "../../Sass/Components/_parallaxe.scss";

import Eau from "../../Img/illustration/eau.gif"

function Home() {  
  const [props, set] = useSpring(() => ({ xy: [0, 0], config: { mass: 10, tension: 550, friction: 140 } }))
  const calc = (x, y) => [x - window.innerWidth / 2, y - window.innerHeight / 2]

  // Losange
  const trans1 = (x, y) => `translate3d(${x / 10}px,${y / 10}px,0)`
  // Cercle Blur
  const trans2 = (x, y) => `translate3d(${x / 8 + 35}px,${y / 8 - 230}px,0)`
  // Triangle
  const trans4 = (x, y) => `translate3d(${x / 3.5}px,${y / 3.5}px,0)`
  // Losange
  const trans5 = (x, y) => `translate3d(${x / 2 + 400}px,${y / 8 - 230}px,0)`
  // Triangle Blur
  const trans6 = (x, y) => `translate3d(${x / 4 + 600}px,${y / 8 - 20}px,0)`
  
    return (
      <section id="home">
        <div className="container">
          <div className="row">
            <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <header>
                <Link className="cta" to="informations" spy={true} smooth={true} duration={2500} >
                  Les bons gestes me concernant
                </Link>

                <Link className="cta" to="animal" spy={true} smooth={true} duration={2500} >
                  Les bons gestes pour mon animal
                </Link>

                <Link className="cta" to="numeros" spy={true} smooth={true} duration={2500} >
                  Qui contacter
                </Link>
                </header>
            </div>
          </div>
          <div className="row full" onMouseMove={({ clientX: x, clientY: y }) => set({ xy: calc(x, y) })}>
            <div className="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 align-self-center">
              {/* <header>
              <Link className="cta" to="informations" spy={true} smooth={true} duration={2500} >
                Les bons gestes me concernant
              </Link>

              <Link className="cta" to="animal" spy={true} smooth={true} duration={2500} >
                Les bons gestes pour mon animal
              </Link>

              <Link className="cta" to="numeros" spy={true} smooth={true} duration={2500} >
                Qui contacter
              </Link>
              </header> */}
              <h1>Rafraîchissez-vous !</h1>
                <animated.div className="icon1" style={{ transform: props.xy.interpolate(trans1) }} />
                <animated.div className="icon2" style={{ transform: props.xy.interpolate(trans2) }} />
              <p><span>Canicule</span> et <span>fortes chaleurs</span>, adoptez les bonnes pratiques pour votre santé. Évitez tout danger pour votre santé lorsque la température est plus élevée que la température habituelle de votre région.</p>
                <Link className="cta-full" to="informations" title="Consulter" spy={true} smooth={true} duration={2500} >
                  Consulter les gestes 
                </Link>
            </div>
            <div className="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 align-self-center">
              <img src={Eau} alt="eau"/>
                <animated.div className="icon4" style={{ transform: props.xy.interpolate(trans4) }} />
                <animated.div className="icon5" style={{ transform: props.xy.interpolate(trans5) }} />
                <animated.div className="icon6" style={{ transform: props.xy.interpolate(trans6) }} />
            </div>
          </div>
        </div>
      </section>
    );
  }

export default Home