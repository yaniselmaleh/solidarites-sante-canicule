import React from "react";
import { useSpring, animated } from 'react-spring'
import "../../Sass/Components/_animal.scss";

import Picto from "../../Img/icone/animal.png"
import Thumbd from "../../Img/illustration/thumbd.png"
import Thumbu from "../../Img/illustration/thumbu.png"

function Animal() {  
        const [props, set] = useSpring(() => ({ xy: [0, 0], config: { mass: 10, tension: 550, friction: 140 } }))
        const calc = (x, y) => [x - window.innerWidth / 2, y - window.innerHeight / 2]

        // Losange
        const trans1 = (x, y) => `translate3d(${x / 10}px,${y / 10}px,0)`
        // Cercle Blur
        const trans2 = (x, y) => `translate3d(${x / 8 + 35}px,${y / 8 - 230}px,0)`
        // Triangle
        const trans4 = (x, y) => `translate3d(${x / 3.5}px,${y / 3.5}px,0)`
        // Losange
        const trans5 = (x, y) => `translate3d(${x / 2 + 400}px,${y / 8 - 230}px,0)`
        // Triangle Blur
        const trans6 = (x, y) => `translate3d(${x / 4 + 600}px,${y / 8 - 20}px,0)`

        return(
            <section id="animal" onMouseMove={({ clientX: x, clientY: y }) => set({ xy: calc(x, y) })}>
                <div id="background"></div>
                <img src={Picto} className="picto" id="picto-animal" alt="picto" />
                <h2>Et pour mon animal</h2>
                
                
                <p id="txt">Les chiens et les chats ne transpirent pratiquement pas, contrairement à nous,<br />
                    ils ne peuvent pas ainsi réguler la température de leur corps.</p>
                    <div className="container">
                        <div className="row">
                        <div className="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <animated.div className="icon1" style={{ transform: props.xy.interpolate(trans1) }} />
                            <animated.div className="icon2" style={{ transform: props.xy.interpolate(trans2) }} />
                            <animated.div className="icon4" style={{ transform: props.xy.interpolate(trans4) }} />
                            <animated.div className="icon6" style={{ transform: props.xy.interpolate(trans6) }} />
                        </div>

                        <div className="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <animated.div className="icon4" style={{ transform: props.xy.interpolate(trans4) }} />
                            <animated.div className="icon5" style={{ transform: props.xy.interpolate(trans5) }} />
                            <animated.div className="icon6" style={{ transform: props.xy.interpolate(trans6) }} />
                        </div>
                            <div className="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 d-flex">
                                <div className="card card-body flex-fill">
                                    <img src={Thumbd} className="illu" alt="picto" />
                                    <h4 className="card-title">Ne laissez jamais votre animal enfermé dans une voiture</h4>
                                    <p className="card-text"><b>Ne le laissez jamais à l’intérieur, même si votre course est rapide !</b> En effet, la température grimpe rapidement dans votre véhicule (même à l’ombre ou fenêtres entrouvertes).</p>

                                    <h4 className="card-title">Pas de balades entre 11 h &amp; 16 h</h4>
                                    <p className="card-text">Il est conseillé de promener votre animal le <b>matin</b> ou le <b>soir</b> où la température est plus bas. Les moments les plus chauds sont entre 11 h et 16 h et il faut les éviter.</p>

                                    <h4 className="card-title">Attentions aux chiens à risque</h4>
                                    <p className="card-text">Évitez d’exposer vos chiens âgés à la chaleur. Ils sont notamment plus sensibles s'ils souffrent de <b>diverses pathologies</b>. C’est la même chose pour les races à museau aplati.</p>

                                    <h4 className="card-title">Évitez les activités sportifs</h4>
                                    <p className="card-text">Privilégiez des <b>activités ludiques</b> en intérieur pour votre animal. Rester chez soi permet de <b>préserver la santé de votre animal</b>.</p>
                                </div>

                            </div>

                            <div className="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 d-flex">

                                <div className="card card-body flex-fill">
                                    <img src={Thumbu} className="illu" alt="picto" />
                                    <h4 className="card-title">Mouillez-votre animal</h4>
                                    <p className="card-text">Afin de préserver la santé de votre animal, il est plus efficace de <b>mouiller les pattes ainsi que les coussinets, le ventre et le creux des cuisses</b>. Vous pouvez toujours mouiller le dos ou la tête également.</p>

                                    <h4 className="card-title">Changez son eau</h4>
                                    <p className="card-text">Faites en sorte que votre animal ait toujours de l’<b>eau à disposition</b> même quand il est seul à la maison ou bien quand vous êtes en balade. <b>Changez également son eau régulièrement.</b></p>

                                    <h4 className="card-title">Assurez-vous qu’il ait de l’ombre</h4>
                                    <p className="card-text"><b>Protégez un maximum votre animal en le mettant à l’ombre quand vous êtes dehors.</b> Il faut éviter qu’il soit trop exposé au soleil.</p>

                                    <h4 className="card-title">Profitez de ces bons moments</h4>
                                    <p className="card-text">Tout en jouant avec votre animal, il est important de <b>s’amuser et de préserver notre santé ainsi que celui de notre animal</b>. Faîtes des <b>activités aquatiques</b> avec votre animal pour vous rafraîchir également.</p>
                                </div>
                            </div>

                        </div>
                    </div>
            </section>
        );
    }

export default Animal