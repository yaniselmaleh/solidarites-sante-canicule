import React from "react";
import "../../Sass/Components/_footer.scss";

const Footer = () => {
    return ( 
        <footer id="footer">
            <p>Créé avec <span role="img" aria-label="coeur">&#128150;</span> par 
                <a href="https://www.linkedin.com/in/yaniselmaleh/" rel="noopener noreferrer" title="Yanis Elmaleh" target="_blank"> Yanis Elmaleh</a>,
                <a href="https://www.linkedin.com/in/jay-alfred-razonable-a2893190/" rel="noopener noreferrer" title="Jay Alfred Razonable" target="_blank"> Jay Alfred Razonable</a> et
                <a href="https://www.linkedin.com/in/mahdi-berlingen/" rel="noopener noreferrer" title="Mahdi Berlingen" target="_blank"> Mahdi Berlingen</a>
            </p> 
        </footer>
    );
}
 
export default Footer;