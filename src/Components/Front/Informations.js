import React from "react";
import "../../Sass/Components/_informations.scss";
import "../Layout/DataCard"
import Fade from 'react-reveal/Fade';

import Water from "../../Img/icone/water.png"
import Gouttes from "../../Img/icone/gouttes.png"
import Interdit from "../../Img/icone/interdit.png"
import Mask from "../../Img/icone/mask.png"
import Profile from "../../Img/icone/profile.png"
import Vent from "../../Img/icone/vent.png"

import Alcool from "../../Img/illustration/alcool.png"
import Corona from "../../Img/illustration/corona.png"
import Eau from "../../Img/illustration/eau.png"
import Messages from "../../Img/illustration/messages.png"
import Neige from "../../Img/illustration/neige.png"
import Pshit from "../../Img/illustration/pshit.png"
// import DataCard from "../Layout/DataCard";



export default class Informations extends React.Component {
    render(){
        return(
            <section id="informations">
                <span></span>
                <div className="container">
                    {/* <DataCard 
                        title="Exemple" text="text d'exemple" icon={Water} className="picto" alt="Title"
                        illu={Pshit} className2="illustration" alt2="Title"
                    /> */}
                    <Fade top>
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                                <h2>Buvez de l’eau, mais pas trop</h2>
                                <p>En ces temps de canicule, il faut boire <b>régulièrement</b> de l’eau. Il est recommandé de boire entre 1 et 1,5 litres d’eau par jour. Mais attention à <b>ne pas boire avec excès</b> pour rester en bonne santé. De plus, mieux vieux un bon verre d’eau à <b>température ambiante</b> qu’une eau bien glacée. </p>
                            </div>

                            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                                <img src={Water} className="picto" alt="picto"/>
                            </div>

                            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                                <img src={Eau} className="illustration" alt="eau"/>
                            </div>
                        </div>
                    </Fade>

                    <Fade top delay={500}>
                    <div className="row">
                        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                            <img src={Alcool} className="illustration" alt="eau"/>
                        </div>

                        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                            <img src={Interdit} className="picto" alt="picto"/>
                        </div>

                        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                            <h2>L’alcool ce n’est pas de l’eau</h2>
                            <p>Il est vrai que l’alcool met l’eau à la bouche, mais nous ne le recommandons pas car il augmente la sécrétion urinaire. Pour remplacer l’alcool, mangez plutôt <b>5 fruits et légumes</b> ! Les laitages seront aussi vos amis.</p>
                        </div>
                    </div>
                    </Fade>

                    <Fade top delay={500}>
                    <div className="row">
                        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                            <h2>Mouillez-vous le corps</h2>
                            <p>Il faut mouiller son corps <b>régulièrement</b> afin de se rafraîchir. Profitez de douches mais pas à l’eau froide pour éviter le choc thermique !
                            </p>
                        </div>

                        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                            <img src={Gouttes} className="picto" alt="picto"/>
                        </div>

                        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                            <img src={Pshit} className="illustration" alt="eau"/>
                        </div>
                    </div>
                    </Fade>


                    <Fade top delay={500}>
                    <div className="row">
                        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                            <img src={Neige} className="illustration" alt="eau"/>
                        </div>

                        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                            <img src={Vent} className="picto" alt="picto"/>
                        </div>

                        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                            <h2>Restez au frais</h2>
                            <p>Le jour, gardez les volets, rideaux et fenêtres fermés. Ouvrez-les seulement la nuit s’il fait un peu plus frais. En ces temps de Covid19, <b>restez chez vous aux heures les plus chaudes.</b> Être à l’abri du soleil permet également de vous garder au frais.</p>
                        </div>
                    </div>
                    </Fade>

                    <Fade top delay={500}>
                    <div className="row">
                        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                            <h2>Maintenez les gestes barrières</h2>
                            <p>Malgré la canicule, il faut <b>respecter les gestes barrières</b> car vous n’êtes pas l’abri du <b>Covid19</b>. Continuez de porter vos masques et de garder une distanciation physique. N’humidifiez jamais votre masque car il deviendra inefficace. </p>
                        </div>

                        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                            <img src={Mask} className="picto" alt="picto"/>
                        </div>

                        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                            <img src={Corona} className="illustration" alt="eau"/>
                        </div>
                    </div>
                    </Fade>

                    <Fade top delay={500}>
                    <div className="row">
                        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                            <img src={Messages} className="illustration" alt="eau"/>
                        </div>

                        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                            <img src={Profile} className="picto" alt="picto"/>
                        </div>

                        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                            <h2>Contactez vos proches</h2>
                            <p><b>Prenez des nouvelles de vos proches</b>, voir comment ils font face à cette canicule, qui sait, peut-être que l’un d’eux a une idée ingénieuse pour faire face à la chaleur.</p>
                        </div>
                    </div>
                    </Fade>
                </div>
            </section>
        )
    }
}