import React from "react";
import "../../Sass/Components/_numeros.scss";

import Picto from "../../Img/icone/temperature.png"
import Footer from "./Footer";


export default class Numeros extends React.Component {
    render(){
        return(
            <section id="numeros">
                <div className="container">
                    <div className="row">
                        <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <img src={Picto} className="picto" id="picto-animal" alt="picto" />
                            <div>
                                <h2>Nausées, convulsions, perte de connaissance<br/><span>appelez le 15</span></h2>
                            <p><b>Si des signes d’alerte se manifestent (crampes, fatigue et/ou insomnie inhabituelle)</b>, buvez de l’eau et mettez-vous au frais. Si votre cas s’aggrave, <b>appelez un médecin</b> car il vaut mieux prévenir que guérir et éviter des pertes de connaissance, convulsions, nausées etc.</p>
                            </div>

                            <div>
                                <h2>Votre animal peut aussi en souffrir<br/><span>appelez le 15</span></h2>
                            <p>Votre animal peut avoir des difficultés respiratoires à cause de la canicule. Les symptômes qui peuvent se manifester sont : <b>une démarche titubante, un état d’agitation qui avec un état d’abattement et une prostration</b>. Appelez également un professionnel pour préserver la santé de votre animal.</p>
                            </div>

                            <div>
                            <h2>Pour plus d’informations, veuillez contacter le<br/><span>0 800 06 66 66</span></h2>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer/>
            </section>
        )
    }
}